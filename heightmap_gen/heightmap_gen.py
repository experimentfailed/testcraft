# Simple way to visiualize heightmaps as they'll be generated in the
# game. You'll probably have to tweak the viewImage code for your needs.
# Eventually I'll have it display a preview before saving.
import os, sys, random, math

from time import time

from PIL import Image
from noise import snoise2

from util import *
from heightmap_terrain import *

fileName = "/home/statik/test1.png"
viewer = "gpicview"

sizex = 2048 # Image Size
sizey = 2048

noise = snoise2


def viewImage(fileName, viewer = viewer):
    os.system("%s %s" % (viewer, fileName))


def colorRampedFBM(ox = 0, oy = 0, scale = 0.005, octaves = 8,
persistence = 0.5, lacunarity = 4.0, rep = 1024, base = 12.32,
ramp = geoRamp(), rampReverse=False):
    r = -1 if rampReverse else 1

    pixels = tuple(
        ramp[int(remapVal(
            noise(
                (x+ox)*scale, (y+oy)*scale,
                octaves, persistence, lacunarity,
                rep, rep,
                base
            )
        ) * r)]

        for x in range(-sizex//2,sizex//2)
            for y in range(-sizey//2,sizey//2)
    )

    img = Image.new("RGB", (sizex, sizey))
    img.putdata(pixels)

    return img


def greyScaleFBM(ox = 0, oy = 0, scale = 0.0005, octaves = 4,
persistence = 0.2, lacunarity = 4.0, rep = 1024, base = 12.32):
    """Generates a greyscale Fractal Browning Motion image"""
    pixels = tuple(
        remapVal(
            noise(
                (x+ox)*scale, (y+oy)*scale,
                octaves, persistence, lacunarity,
                rep, rep,
                base
            ),
            out_min = 0,
            out_max = 255
        )

        for x in range(-sizex//2,sizex//2)
            for y in range(-sizey//2,sizey//2)
    )

    img = Image.new("L", (sizex, sizey))
    img.putdata(pixels)

    return img


def colorRampedBiomes(ox=0, oz=0, ramp=geoRamp(), rampReverse=False):
    r = -1 if rampReverse else 1

    pixels = tuple(
        ramp[biomeNoise(x+ox,z+oz)[0] * r]

        for x in range(-sizex//2,sizex//2)
            for z in range(-sizey//2,sizey//2)
    )

    img = Image.new("RGB", (sizex, sizey))
    img.putdata(pixels)

    return img


def greyScaleBiomes(ox=0,oz=0):
    pixels = tuple(
        biomeNoise(x+ox, z+oz)[0]

        for x in range(-sizex//2,sizex//2)
            for z in range(-sizey//2,sizey//2)
    )

    img = Image.new("L", (sizex, sizey))
    img.putdata(pixels)

    return img

def colorBiomes(ox=0, oz=0):
    pixels = tuple(
        biomeNoise(x+ox, z+oz)[1]

        for x in range(-sizex//2,sizex//2)
            for z in range(-sizey//2,sizey//2)
    )

    img = Image.new("RGB", (sizex, sizey))
    img.putdata(pixels)

    return img

# What the hell biome combination is at ox=8192 o_O
img = colorBiomes(sizex*4)

img.save(fileName.replace('\\', ''))
viewImage(fileName)
