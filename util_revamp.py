import noise, random
from math import sqrt, floor

# Most of this will be done in a config file later
SEED = 2.35 # This isn't ideal. Noise module doesn't seem to have a
# typical seed setting, so using the 'base' argument for now.

TICKS_PER_SEC = 30

# Sectors are NxNxN blocks
SECTOR_SIZE = 16

# Num sectors to draw around player in each cardinal direction
VIEW_DISTANCE = 16

WALKING_SPEED = 5
FLYING_SPEED = 15

GRAVITY = 20.0
MAX_JUMP_HEIGHT = 1.0 # About the height of a block.

JUMP_SPEED = sqrt(2 * GRAVITY * MAX_JUMP_HEIGHT)
TERMINAL_VELOCITY = 50

PLAYER_HEIGHT = 2

FAR_CLIP = SECTOR_SIZE * VIEW_DISTANCE
FOG_START = SECTOR_SIZE * (VIEW_DISTANCE / 2)
FOG_END = FAR_CLIP - SECTOR_SIZE

# The rest of this file is just utility functions & convenience helpers
# for the rest of the engine & procedural generators
random.seed(SEED)

noise1 = noise.pnoise1
noise2 = noise.snoise2
noise3 = noise.snoise3
choice = random.choice
choices = random.choices
randint = random.randint
randfloat = random.random

def tex_coord(x, y, n=4):
    """ Return the bounding vertices of the texture square.
    """
    m = 1.0 / n

    dx = x*m
    dy = y*m

    return dx, dy, dx+m, dy, dx+m, dy+m, dx, dy+m


def tex_coords(top, bottom, side):
    """ Return a list of the texture squares for the top, bottom and
    side.
    """
    top = tex_coord(*top)
    bottom = tex_coord(*bottom)
    side = tex_coord(*side)

    result = []

    result.extend(top)
    result.extend(bottom)
    result.extend(side * 4)

    return result


def cube_vertices_tuples(x, y, z, n=1):
    """ Return the vertices of the cube at position x, y, z with size
    2*n.
    """
    return [
        (x-n,y+n,z-n), (x-n,y+n,z+n), (x+n,y+n,z+n), (x+n,y+n,z-n),  # top
        (x-n,y-n,z-n), (x+n,y-n,z-n), (x+n,y-n,z+n), (x-n,y-n,z+n),  # bottom
        (x-n,y-n,z-n), (x-n,y-n,z+n), (x-n,y+n,z+n), (x-n,y+n,z-n),  # left
        (x+n,y-n,z+n), (x+n,y-n,z-n), (x+n,y+n,z-n), (x+n,y+n,z+n),  # right
        (x-n,y-n,z+n), (x+n,y-n,z+n), (x+n,y+n,z+n), (x-n,y+n,z+n),  # front
        (x+n,y-n,z-n), (x-n,y-n,z-n), (x-n,y+n,z-n), (x+n,y+n,z-n),  # back
    ]

def cube_vertices(x, y, z, n=1):
    """ Return the vertices of the cube at position x, y, z with size
    2*n.
    """
    return [
        x-n,y+n,z-n, x-n,y+n,z+n, x+n,y+n,z+n, x+n,y+n,z-n,  # top
        x-n,y-n,z-n, x+n,y-n,z-n, x+n,y-n,z+n, x-n,y-n,z+n,  # bottom
        x-n,y-n,z-n, x-n,y-n,z+n, x-n,y+n,z+n, x-n,y+n,z-n,  # left
        x+n,y-n,z+n, x+n,y-n,z-n, x+n,y+n,z-n, x+n,y+n,z+n,  # right
        x-n,y-n,z+n, x+n,y-n,z+n, x+n,y+n,z+n, x-n,y+n,z+n,  # front
        x+n,y-n,z-n, x-n,y-n,z-n, x-n,y+n,z-n, x+n,y+n,z-n,  # back
    ]


def normalize(x,y,z):
    """ Accepts `position` of arbitrary precision and returns the block
    containing that position.
    """
    return (int(round(x)), int(round(y)), int(round(z)))


def sectorize(x,y,z):
    """Returns the sector for a given position
    """
    x, y, z = normalize(x,y,z)

    return (x // SECTOR_SIZE, y // SECTOR_SIZE, z // SECTOR_SIZE)


def remapVal(val, in_min=-1, in_max=1, out_min=0, out_max=127):
    """Translates a value from one range to another
    """
    return (val-in_min) * (out_max-out_min) / (in_max-in_min) + out_min
