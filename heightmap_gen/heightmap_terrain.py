from math import floor

from noise import snoise2 as noise2

biomes = [
    # Octaves, persist, lac, scale, ymin, ymax, red, green, blue
    (4, 0.5, 4.0, 0.003, 128, 256, 33, 33, 33), # Mountains
    (4, 0.5, 4.0, 0.001, 64, 128, 34, 25, 22), # Low mountains
    (3, 0.5, 3.0, 0.007, 16, 64, 38, 27, 14), # Rugged hills
    (2, 0.5, 2.0, 0.01, 8, 32, 12, 52, 48), # Rolling hills
    (2, 0.5, 2.0, 0.0075, 0, 16, 9, 43, 6), # Plains
]

n = len(biomes)

rep = 131072
mns = 0.0005 # Master noise scale
brightness = 75

SEED = 2.35


def remapVal(val, in_min=-1, in_max=1, out_min=0, out_max=127):
    """Returns val as it would be if the range were scaeled from
    in_min/max to out_min/max.
    """
    return (val-in_min) * (out_max-out_min) / (in_max-in_min) + out_min


def biomeNoise(x, z):
    # Adopted https://parzivail.com/procedural-terrain-generaion/

    l = (noise2(x*mns, z*mns, 4, 0.2, 4.0, rep, rep, SEED) + 1) / 2

    y = 0
    red = 0
    green = 0
    blue = 0

    for i, b in enumerate(biomes):
        if ((i - 1.0) / n <= l and l <= (i + 1.0) / n):
            ns = b[3]

            w = (-abs(n * l - i) + 1)

            y += w * remapVal(
                noise2(
                    x*ns, z*ns,
                    b[0], b[1], b[2],
                    rep, rep,
                    SEED
                ),
                out_min=b[4],
                out_max=b[5]
            )

            red += b[6] * w
            green += b[7] * w
            blue += b[8] * w

    return ( int(y), (int(red+brightness), int(green+brightness),
    int(blue+brightness)) )




