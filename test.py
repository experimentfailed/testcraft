from noise import snoise3 as noise3
from noise import snoise2 as noise2

from time import time

from util_revamp import *

SECTOR_SIZE = 16 # Sectors are NxNxN blocks
VIEW_DISTANCE = 16 # Draw NxNxN sectors around player

sector_cache = []
sector_append = sector_cache.append
vertex_cache = {}

def generate_sector(x,y,z):
    blocks = []
    blocks_append = blocks.append

    sx,sy,sz = x*SECTOR_SIZE,y*SECTOR_SIZE,z*SECTOR_SIZE

    sn = noise3(sx*0.00001,sy*0.00001,sz*0.00001)
    ss = remapVal(sn, -1,1, 0.001, 0.005) # surface noise scale
    ns = remapVal(sn, -1,1, 0.01, 0.05) # main noise scale

    for x in range(sx, sx+SECTOR_SIZE):
        for y in range(sy, sy+SECTOR_SIZE):
            for z in range(sz, sz+SECTOR_SIZE):
                # Surface ceiling - no blocks above this limit
                if y > remapVal(noise2(x*ss,z*ss,4), -1,1, -255, 255):
                    continue

                xns, yns, zns = x*ns, y*ns, z*ns
                octaves = 4
                th = 0 # Noise threshold

                # Get noise value for current block & check if any
                # surrounding block is missing. If block has missing
                # neighbor (aka exposed), cache it; else skip.
                if not noise3(xns, yns, zns, octaves) > th:
                    if not (noise3(xns-ns, yns, zns, octaves) > th
                    and noise3(xns, yns-ns, zns, octaves) > th
                    and noise3(xns, yns, zns-ns, octaves) > th
                    and noise3(xns+ns, yns, zns, octaves) > th
                    and noise3(xns, yns+ns, zns, octaves) > th
                    and noise3(xns,yns,zns+ns, octaves) > th):
                        # block color...Should be able to pack this into
                        # a single byte
                        blocks_append((255,255,255,255))

                        for v in cube_vertices(x,y,z):
                            if v not in vertex_cache:
                                vertex_cache[v] = None

    return blocks


ts = time()

for x in range(-VIEW_DISTANCE,VIEW_DISTANCE+1):
    for y in range(-2,VIEW_DISTANCE+1):
        for z in range(-VIEW_DISTANCE,VIEW_DISTANCE+1):
            sector_append(generate_sector(x,y,z))


print("List Time: ", time() - ts)

total_blocks = 0
for s in sector_cache:
    total_blocks += len(s)

print(total_blocks)        

while True: # Keep alive so can see final memory usage according to OS
    continue
