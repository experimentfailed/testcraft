from math import floor

from util import noise2, noise3, SEED, remapVal, SECTOR_SIZE

biomes = ( # Noise params representing different biomes
#    Oct, per, lac, scale, ymin, ymax,  ns,   nt
#    0    1    2    3      4      5     6      7
    (8,   0.5, 4.0, 0.003, 128,   512,  0.01, -0.03 ), # Rugged Mountains
    (6,   0.5, 4.0, 0.003, 128,   256,  0.01, -0.05),  # Mountains
    (4,   0.5, 4.0, 0.001,  64,   128,  0.01, -0.01 ), # Low mountains
    (4,   0.5, 3.0, 0.007,  16,    64,  0.01,  0.01 ), # Rugged hills
    (6,   0.5, 2.0, 0.01,    8,    16,  0.01,  0.05),  # Rolling hills
    (8,   0.5, 2.0, 0.0075,  0,     4,  0.01,  0.03 ), # Plains
)

numBiomes = len(biomes)

mns = 0.0005 # Master noise scale

def getBlockStack(x,z):
    """Returns a block height 'y' for a given x,z, with gradients
    between biome definitions. Essentially generates a terrain surface.
    """
    # Adopted from https://parzivail.com/procedural-terrain-generaion/
    l = remapVal(
        noise2(
            x*mns, z*mns,
            4, 0.2, 4.0, # Octaves=1, persistence=0.5, lacunarity=2.0
            base=SEED
        ),
        out_min=0,
        out_max=1
    )

    y = 0
    bt = 0
    bs = 0
    raw = 0

    for i, b in enumerate(biomes): # Establish final block height
        if ((i - 1.0) / numBiomes <= l and l <= (i + 1.0) / numBiomes):
            ns = b[3] # Noise Scale

            rn = noise2(
                x*ns, z*ns,
                b[0], b[1], b[2],
                base=SEED
            )

            y += (-abs(numBiomes * l - i) + 1) * remapVal(
                rn,
                out_min=b[4],
                out_max=b[5]
            )

            bs += b[6]
            bt += b[7]
            raw += rn

    xns, zns = x*bs, z*bs

    blocks = []
    blocks_append = blocks.append

    y = int(y)

    for y in range(y-1, y-16, -1):
        # ~ yns = y*bs

        # ~ n = noise3(
            # ~ xns, yns, zns,
            # ~ int( remapVal(bs, out_min=1, out_max=8) ),
            # ~ bt,
            # ~ remapVal(raw, out_min=-60, out_max=60)
        # ~ )

        blocks_append((x, y, z))

    return blocks
