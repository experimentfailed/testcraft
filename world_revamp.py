

# Memroy usage is out of control

from collections import deque
from time import time, perf_counter
import sys

# Pyglet
from pyglet import image
from pyglet.graphics import Batch, TextureGroup
from pyglet.gl import GL_QUADS

# Testcraft
from util_revamp import *

# A Batch is a collection of vertex lists for batched rendering.
batch = Batch()

texGroup = None

TEXTURE_FILE = 'texture.png'

BLOCK_TYPES = [
    tex_coords((1, 0), (0, 1), (0, 0)), # GRASS
    tex_coords((0, 1), (0, 1), (0, 1)), # DIRT
    tex_coords((0, 2), (0, 2), (0, 2)), # STONE
    tex_coords((1, 1), (1, 1), (1, 1)), # SAND
    tex_coords((2, 1), (2, 1), (2, 1)), # STONE_BRICK
    tex_coords((2, 0), (2, 0), (2, 0))  # BRICK
]

def world_init():
    global texGroup

    # A TextureGroup manages an OpenGL texture.
    texGroup = TextureGroup(
        image.load(TEXTURE_FILE).get_texture()
    )

FACES = (
    ( 0, 1, 0),
    ( 0,-1, 0),
    ( 1, 0, 0),
    (-1, 0, 0),
    ( 0, 0, 1),
    ( 0, 0,-1)
)

# Cache of blocks on screen
sector_cache = {}

# Modifications to the world; eventually can pickle this for save games.
# Will be structured like sector_cache, but sectors only contain placed
# and removed blocks.
mod_cache = {}

queue = {}


def generate_block(x,y,z):
    pass


def get_block(x,y,z):
    """Find & return block at x,y,z, if any. Searches sector_cache
    first, then mod_cache. If still no block, generates new from noise.
    """
    # This all "works" but math not 100% verified
    sx,sy,sz = sectorize(x,y,z)

    bx = x-(sx*SECTOR_SIZE)
    by = y-(sy*SECTOR_SIZE)
    bz = z-(sz*SECTOR_SIZE)

    block = None

    sector = sector_cache.get((sx,sy,sz))

    if sector:
        block = sector.get((bx,by,bz))
    else:
        sector = mod_cache.get((sx,sy,sz))

        if sector:
            block = sector.get((bx,by,bz))

    return block or generate_block(bx,by,bz)
    

def count_cached_blocks(sources=(sector_cache, mod_cache)):
    total = 0
    for cache_source in sources:
        for sector in cache_source:
            total += len(sector)
        
    return total
    

def add_block(x,y,z, color):
    """Add a block at x,y,z
    """
    # WIP
    sx,sy,sz = sectorize(x,y,z)

    sector = sector_cache.get(x,y,z)

    if not sector:
        sector_cache[sx,sy,sz] = {(x,y,z):{
                "verts":batch.add(
                    24,
                    GL_QUADS,
                    texGroup,
                    ('v3f/static', cube_vertices(x,y,z, 0.5)),
                    ('c4B/static', color)
                )
        }}


def remove_block(x,y,z):
    """Remove block at x,y,z
    """
    pass


def cull_sectors(x,y,z):
    """Find & remove any/all sectors outside view distance from x,y,z
    origin
    """
    # This seems to be complete but more testing needed later, after the
    # revamp can start rendering. Not entirely sure how this will behave 
    # in Pyglet
    remove = []
    for s in sector_cache:
        if not (abs(s[0]) > VIEW_DISTANCE+x
        and abs(s[1]) > VIEW_DISTANCE+y
        and abs(s[2]) > VIEW_DISTANCE+z):
            remove.append(s)

    for s in remove:
        sector_cache.pop(s)


def generate_sector(x,y,z):
    """Generate a new sector from noise at x,y,z
    """
    # WIP...will eventually bring over the world_cubic algorithm and
    # expand on that
    blocks = {}

    ns = 0.1
    th = 0.1
    for bx in range(SECTOR_SIZE):
        for by in range(SECTOR_SIZE):
            for bz in range(SECTOR_SIZE):
                sx = bx + x
                sy = by + y
                sz = bz + z

                sxn, syn, szn = sx * ns, sy * ns, sz * ns

                if not noise3(sxn,syn,szn) > th:
                    if not (noise3(sxn-ns,syn,szn) > th
                    and noise3(sxn,syn-ns,szn) > th
                    and noise3(sxn,syn,szn-ns) > th
                    and noise3(sxn+ns,syn,szn) > th
                    and noise3(sxn,syn+ns,szn) > th
                    and noise3(sxn,syn,szn+ns) > th):
                        blocks[bx,by,bz] = (
                            (100,100,100,255),
                            None
                        )

    return blocks


ts = time()

for x in range(-VIEW_DISTANCE, VIEW_DISTANCE+1):
    for y in range(-VIEW_DISTANCE, VIEW_DISTANCE+1):
        for z in range(-VIEW_DISTANCE, VIEW_DISTANCE+1):
            sector_cache[x,y,z] = generate_sector(x,y,z)


print("Time", time() - ts)
print("Num Sectors", len(sector_cache))
print("Num Blocks", count_cached_blocks())
print(get_block(8,1,7))

size = 0
for i in sector_cache:
    size += sys.getsizeof(i)
    
print(size)

while True:
    pass
# ~ print()

# ~ ts = time()
# ~ while VIEW_DISTANCE >= -1:
    # ~ cull_sectors(0,0,0)
    # ~ VIEW_DISTANCE -= 1

# ~ print("Time",time() - ts)
# ~ print(len(sector_cache))


# ~ ts = time()
# ~ for i in range(100000):
    # ~ get_block(0,0,0)
# ~ print(time()-ts)
