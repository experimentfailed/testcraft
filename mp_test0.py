from time import time
from noise import snoise3 as noise3

SECTOR_SIZE = 16
VIEW_DISTANCE = 8

sector_cache = {}


def normalize(x,y,z):
    return (int(round(x)), int(round(y)), int(round(z)))


def sectorize(x,y,z):
    """Returns the sector for a given position"""
    x, y, z = normalize(x,y,z)

    return (x // SECTOR_SIZE, y // SECTOR_SIZE, z // SECTOR_SIZE)


def remapVal(val, in_min=-1, in_max=1, out_min=0, out_max=127):
    """Returns val as it would be if the range were scaeled from
    in_min/max to out_min/max.
    """
    return (val-in_min) * (out_max-out_min) / (in_max-in_min) + out_min


def get_block(x,y,z):
    sx,sy,sz = sectorize(x,y,z)

    x = x-(sx*SECTOR_SIZE)
    y = y-(sy*SECTOR_SIZE)
    z = z-(sz*SECTOR_SIZE)

    return sector_cache[sx,sy,sz][x][y][z]


def num_blocks():
    return (SECTOR_SIZE**3) * len(sector_cache)


def iter_blocks():
    for sector in sector_cache.values():
        for x in sector:
            for y in x:
                for block in y:
                    yield block


def generate_sector(x,y,z, ns=0.1):
    blocks = [[[(None, False)]*SECTOR_SIZE]*SECTOR_SIZE]*SECTOR_SIZE

    for x in range(SECTOR_SIZE):
        for y in range(SECTOR_SIZE):
            for z in range(SECTOR_SIZE):
                sx,sy,sz = sectorize(x,y,z)

                sx,sy,sz = (sx+x)*ns, (sy+y)*ns, (sz+z)*ns

                if noise3(sx,sy,sz) > 0:
                    # Check if exposed
                    if (noise3(sx-ns,sy,sz) > 0
                    and noise3(sx,sy-ns,sz) > 0
                    and noise3(sx,sy,sz-ns) > 0
                    and noise3(sx+ns,sy,sz) > 0
                    and noise3(sx,sy+ns,sz) > 0
                    and noise3(sx,sy,sz+ns) > 0):
                        vis = False
                    else:
                        vis = True

                    blocks[x][y][z] = ("dirt", vis) #Tex, vis

    return blocks

ts = time()
for x in range(-VIEW_DISTANCE, VIEW_DISTANCE+1):
    for y in range(-VIEW_DISTANCE, VIEW_DISTANCE+1):
        for z in range(-VIEW_DISTANCE, VIEW_DISTANCE+1):
            sector_cache[x,y,z] = generate_sector(x,y,z)

print("Time",time() - ts)
print("Num Sectors",len(sector_cache))
print("Num Block Positions", num_blocks())
print(get_block(-120,97,32))




# ~ import multiprocessing
# ~ from util import *

# ~ SST = SECTOR_SIZE+1

# ~ def task0(pos):
    # ~ blocks = []
    # ~ blocks_append = blocks.append

    # ~ for x in range(pos[0]-SECTOR_SIZE, SST+pos[0]):
        # ~ for y in range(pos[1]-SECTOR_SIZE, SST+pos[1]):
            # ~ for z in range(pos[2]-SECTOR_SIZE, SST+pos[2]):
                # ~ if noise3(x,y,z) > 0:
                    # ~ blocks_append((x,y,z))

    # ~ return blocks
# ~ ts = time()
# ~ VDT = VIEW_DISTANCE+1
# ~ sectors = ((x,y,z)
    # ~ for x in range(-VIEW_DISTANCE, VDT)
    # ~ for y in range(-VIEW_DISTANCE, VDT)
    # ~ for z in range(-VIEW_DISTANCE, VDT)
# ~ )

# ~ pool = multiprocessing.Pool(processes=16)
# ~ blocks = pool.map(task0, sectors)
# ~ pool.close()

# ~ blks = []
# ~ blks_append = blks.append

# ~ for b in blocks:
    # ~ blks += b

# ~ print(time() - ts)
# ~ print(len(blks))


# ~ tssp = time()
# ~ task0()
# ~ task0()
# ~ task0()
# ~ task0()
# ~ tesp = time() - tssp

# ~ tsmp = time()
# ~ # creating processes
# ~ p1 = multiprocessing.Process(target=task0)
# ~ p2 = multiprocessing.Process(target=task0)
# ~ p3 = multiprocessing.Process(target=task0)
# ~ p4 = multiprocessing.Process(target=task0)

# ~ # starting processes
# ~ p1.start()
# ~ p2.start()
# ~ p3.start()
# ~ p4.start()

# ~ # wait until processes are finished
# ~ p1.join()
# ~ p2.join()
# ~ p3.join()
# ~ p4.join()

# ~ print(tesp)
# ~ print(time() - tsmp)
# ~ # both processes finished
# ~ print("Done!")
