#!/bin/python3.7
# Dependencies
# pip3 install --user pyglet==1.5.27
# pip3 install --user noise


# Python native
from math import cos, sin, radians, degrees, atan2

# Pyglet
from pyglet import window, text, app, graphics
from pyglet import clock as pyglet_clock
from pyglet.gl import *

# Testcraft
from world_cubic import *


key, mouse = window.key, window.mouse

num_keys = [ # Convenience list of number keys
    key._1, key._2, key._3, key._4, key._5, key._6, key._7, key._8,
    key._9, key._0
]

class Window(window.Window):
    def __init__(self, *args, **kwargs):
        super(Window, self).__init__(*args, **kwargs)

        # Whether or not the window exclusively captures the mouse.
        self.exclusive = False

        # When flying gravity has no effect and speed is increased.
        self.flying = False

        # Strafing is moving lateral to the direction you are facing,
        # e.g. moving to the left or right while continuing to face
        # forward.
        #
        # First element is -1 when moving forward, 1 when moving back,
        # and 0 otherwise. The second element is -1 when moving left, 1
        # when moving right, and 0 otherwise.
        self.strafe = [0, 0]

        # Current (x, y, z) position in the world, specified with
        # floats. Note that, perhaps unlike in math class, the y-axis is
        # the vertical axis.
        self.position = (8192, 200, 5000)

        # First element is rotation of the player in the x-z plane
        # (ground plane) measured from the z-axis down. The second is
        # the rotation angle from the ground plane up. Rotation is in
        # degrees.
        #
        # The vertical plane rotation ranges from -90 (looking straight
        # down) to 90 (looking straight up). The horizontal rotation
        # range is unbounded.
        self.rotation = (0, 0)

        # Which sector the player is currently in.
        self.sector = None

        # The crosshairs at the center of the screen.
        self.reticle = None

        # Velocity in the y (upward) direction.
        self.dy = 0

        # A list of blocks the player can place. Hit num keys to cycle.
        self.inventory = BLOCK_TYPES

        # The current block the user can place. Hit num keys to cycle.
        self.block = self.inventory[0]

        # The label that is displayed in the top left of the canvas.
        self.label = text.Label(
            '',
            font_name='Arial',
            font_size=18,
            x=10,
            y=self.height - 10,
            anchor_x='left',
            anchor_y='top',
            color=(0, 0, 0, 255)
        )

        world_init()

        # This call schedules the `update()` method to be called
        # TICKS_PER_SEC. This is the main game event loop.
        pyglet_clock.schedule_interval(self.update, 1.0 / TICKS_PER_SEC)

    def set_exclusive_mouse(self, exclusive):
        """If `exclusive` is True, the game will capture the mouse, if
        False the game will ignore the mouse.
        """
        super(Window, self).set_exclusive_mouse(exclusive)
        self.exclusive = exclusive

    def get_sight_vector(self):
        """Returns the current line of sight vector indicating the
        direction the player is looking.
        """
        x, y = self.rotation

        # y ranges from -90 to 90, or -pi/2 to pi/2, so m ranges from 0
        # to 1 and is 1 when looking ahead parallel to the ground and 0
        # when looking straight up or down.
        m = cos(radians(y))

        # dy ranges from -1 to 1 and is -1 when looking straight down
        # and 1 when looking straight up.
        x -= 90
        dy = sin(radians(y))
        dx = cos(radians(x)) * m
        dz = sin(radians(x)) * m

        return (dx, dy, dz)

    def get_motion_vector(self):
        """Returns the current motion vector indicating the velocity of
        the player.

        Returns
        -------
        vector : tuple of len 3
            Tuple containing the velocity in x, y, and z respectively.
        """
        if any(self.strafe):
            x, y = self.rotation
            strafe = degrees(atan2(*self.strafe))
            y_angle = radians(y)
            x_angle = radians(x + strafe)

            if self.flying:
                m = cos(y_angle)
                dy = sin(y_angle)

                if self.strafe[1]:
                    # Moving left or right.
                    dy = 0.0
                    m = 1

                if self.strafe[0] > 0:
                    # Moving backwards.
                    dy *= -1

                # When you are flying up or down, you have less left and
                # right motion.
                dx = cos(x_angle) * m
                dz = sin(x_angle) * m
            else:
                dy = 0.0
                dx = cos(x_angle)
                dz = sin(x_angle)
        else:
            dy = 0.0
            dx = 0.0
            dz = 0.0

        return (dx, dy, dz)

    def update(self, dt):
        """This method is scheduled to be called repeatedly by the
        pyglet clock.

        Parameters
        ----------
        dt : float
            The change in time since the last call.
        """
        currSector = self.sector

        process_queue()
        sector = sectorize(self.position)

        if sector != currSector:
            change_sectors(currSector, sector)

            if currSector is None:
                process_entire_queue()

            self.sector = sector

        m = 8
        dt = min(dt, 0.2)

        for _ in range(m):
            self._update(dt / m)

    def _update(self, dt):
        """Private implementation of the `update()` method. This is
        where most of the motion logic lives, along with gravity and
        collision detection.

        Parameters
        ----------
        dt : float
            The change in time since the last call.
        """
        flying = self.flying

        # walking
        speed = FLYING_SPEED if flying else WALKING_SPEED

        d = dt * speed # distance covered this tick.

        dx, dy, dz = self.get_motion_vector()

        # New position in space, before accounting for gravity.
        dx, dy, dz = dx * d, dy * d, dz * d

        # gravity
        if not flying:
            # Update your vertical speed: if you are falling, speed up
            # until you hit terminal velocity; if you are jumping, slow
            # down until you start falling.
            self.dy -= dt * GRAVITY
            self.dy = max(self.dy, -TERMINAL_VELOCITY)
            dy += self.dy * dt

        # collisions
        x, y, z = self.position
        x, y, z = self.collide((x + dx, y + dy, z + dz), PLAYER_HEIGHT)

        self.position = (x, y, z)

    def collide(self, position, height):
        """Checks to see if the player at the given `position` and
        `height` is colliding with any blocks in the world.

        Parameters
        ----------
        position : tuple of len 3
            The (x, y, z) position to check for collisions at.
        height : int or float
            The height of the player.

        Returns
        -------
        position : tuple of len 3
            The new position of the player taking into account
            collisions.
        """
        # How much overlap with a dimension of a surrounding block you
        # need to have to count as a collision. If 0, touching terrain
        # at all counts as a collision. If .49, you sink into the
        # ground, as if walking through tall grass. If >= .5, you'll
        # fall through the ground.
        pad = 0.25
        p = list(position)
        np = normalize(position)

        for face in FACES:  # check all surrounding blocks
            for i in range(3):  # check each dimension independently
                if not face[i]:
                    continue

                # How much overlap you have with this dimension.
                d = (p[i] - np[i]) * face[i]

                if d < pad:
                    continue

                for dy in range(height):  # check each height
                    op = list(np)
                    op[1] -= dy
                    op[i] += face[i]

                    if tuple(op) not in worldBlocks:
                        continue
                    p[i] -= (d - pad) * face[i]

                    if face == (0, -1, 0) or face == (0, 1, 0):
                        # You are colliding with the ground or ceiling,
                        # so stop falling / rising.
                        self.dy = 0

                    break

        return tuple(p)

    def on_mouse_press(self, x, y, btn, mod):
        """Called when a mouse button is pressed. See pyglet docs for
        button amd modifier mappings.

        Parameters
        ----------
        x, y : int
            The coordinates of the mouse click. Always center of the
            screen if the mouse is captured.
        button : int
            Number representing mouse button that was clicked. 1 = left
            button, 4 = right button.
        modifiers : int
            Number representing any modifying keys that were pressed
            when the mouse button was clicked.
        """
        if self.exclusive:
            vector = self.get_sight_vector()
            block, previous = hit_test(self.position, vector)

            if (btn == mouse.RIGHT
            or ((btn == mouse.LEFT) and (mod & key.MOD_CTRL))):
                # ON OSX, control + left click = right click.
                if previous:
                    add_block(previous, self.block)
            elif btn == window.mouse.LEFT and block:
                texture = worldBlocks[block]

                if texture != BLOCK_TYPES[2]: # Unbreakable block
                    remove_block(block)
        else:
            self.set_exclusive_mouse(True)

    def on_mouse_motion(self, x, y, dx, dy):
        """Called when the player moves the mouse.

        Parameters
        ----------
        x, y : int
            The coordinates of the mouse click. Always center of the
            screen if the mouse is captured.
        dx, dy : float
            The movement of the mouse.
        """
        if self.exclusive:
            m = 0.15
            x, y = self.rotation
            x, y = x + dx * m, y + dy * m
            y = max(-90, min(90, y))
            self.rotation = (x, y)

    def on_key_press(self, symbol, modifiers):
        """Called when the player presses a key. See pyglet docs for key
        mappings.

        Parameters
        ----------
        symbol : int
            Number representing the key that was pressed.
        modifiers : int
            Number representing any modifying keys that were pressed.
        """
        if symbol == key.F:
            self.strafe[0] -= 1
        elif symbol == key.V:
            self.strafe[0] += 1
        elif symbol == key.A:
            self.strafe[1] -= 1
        elif symbol == key.S:
            self.strafe[1] += 1
        # ~ elif symbol == key.G:
            # ~ self.position[2] += 1000
        elif symbol == key.SPACE:
            if self.dy == 0:
                self.dy = JUMP_SPEED
        elif symbol == key.ESCAPE:
            self.set_exclusive_mouse(False)
        elif symbol == key.TAB:
            self.flying = not self.flying
        elif symbol in num_keys:
            index = (symbol - num_keys[0]) % len(self.inventory)
            self.block = self.inventory[index]

    def on_key_release(self, symbol, modifiers):
        """Called when the player releases a key. See pyglet docs for
        key mappings.

        Parameters
        ----------
        symbol : int
            Number representing the key that was pressed.
        modifiers : int
            Number representing any modifying keys that were pressed.
        """
        strafe = self.strafe

        if symbol == key.F:
            strafe[0] += 1
        elif symbol == key.V:
            strafe[0] -= 1
        elif symbol == key.A:
            strafe[1] += 1
        elif symbol == key.S:
            strafe[1] -= 1

    def on_resize(self, width, height):
        """Called when the window is resized to a new `width` and
        `height`.
        """
        # label
        self.label.y = height - 10

        # reticle
        if self.reticle:
            self.reticle.delete()

        x, y = self.width // 2, self.height // 2
        n = 10

        self.reticle = graphics.vertex_list(4,
            ('v2i', (x - n, y, x + n, y, x, y - n, x, y + n))
        )

    def set_2d(self):
        """Configure OpenGL to draw in 2d.
        """
        width, height = self.get_size()
        glDisable(GL_DEPTH_TEST)
        viewport = self.get_viewport_size()
        glViewport(0, 0, max(1, viewport[0]), max(1, viewport[1]))
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glOrtho(0, max(1, width), 0, max(1, height), -1, 1)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

    def set_3d(self):
        """Configure OpenGL to draw in 3d.
        """
        width, height = self.get_size()
        glEnable(GL_DEPTH_TEST)
        viewport = self.get_viewport_size()
        glViewport(0, 0, max(1, viewport[0]), max(1, viewport[1]))
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(65.0, width / float(height), 0.1, FAR_CLIP)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        x, y = self.rotation
        glRotatef(x, 0, 1, 0)
        glRotatef(-y, cos(
            radians(x)), 0, sin(radians(x))
        )
        x, y, z = self.position
        glTranslatef(-x, -y, -z)

    def on_draw(self):
        """Called by pyglet to draw the canvas.
        """
        self.clear()
        self.set_3d()
        glColor3d(1, 1, 1)
        batch.draw()
        self.draw_focused_block()
        self.set_2d()
        self.draw_label()
        self.draw_reticle()

    def draw_focused_block(self):
        """Draw black edges around the block that is currently under the
        crosshairs.
        """
        vector = self.get_sight_vector()

        block = hit_test(self.position, vector)[0]

        if block:
            vertex_data = cube_vertices(*block, 0.51)

            glColor3d(0, 0, 0)

            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)

            graphics.draw(
                24, GL_QUADS, ('v3f/static', vertex_data)
            )

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)

    def draw_label(self):
        """Draw the label in the top left of the screen.
        """
        #~ x, y, z = self.position
        label = self.label

        label.text = '%02d (%.2f, %.2f, %.2f) %d / %d' % (
            pyglet_clock.get_fps(), *self.position,
            len(shownv), len(worldBlocks))

        label.draw()

    def draw_reticle(self):
        """Draw the crosshairs in the center of the screen.
        """
        glColor3d(0, 0, 0)
        self.reticle.draw(GL_LINES)


def setup_fog():
    """Distance fog...it's not right"""

    # Enable fog. Fog "blends a fog color with each rasterized pixel
    # fragment's post-texturing color."
    glEnable(GL_FOG)
    # Set the fog color.
    glFogfv(GL_FOG_COLOR, (GLfloat * 4)(0.5, 0.69, 1.0, 1))
    # Say we have no preference between rendering speed and quality.
    glHint(GL_FOG_HINT, GL_DONT_CARE)
    # Specify the equation used to compute the blending factor.
    glFogi(GL_FOG_MODE, GL_LINEAR)
    # How close and far away fog starts and ends. The closer the start
    # and end, the denser the fog in the fog range.
    glFogf(GL_FOG_START, FOG_START)
    glFogf(GL_FOG_END, FOG_END)


def setup():
    """Basic OpenGL configuration.
    """
    # Set the color of "clear", i.e. the sky, in rgba.
    glClearColor(0.5, 0.69, 1.0, 1)
    # Enable culling (not rendering) of back-facing facets -- facets
    # that aren't visible to you.
    glEnable(GL_CULL_FACE)
    # Set the texture minification/magnification function to GL_NEAREST
    # (nearest in Manhattan distance) to the specified texture
    # coordinates. GL_NEAREST "is generally faster than GL_LINEAR, but
    # it can produce textured images with sharper edges because the
    # transition between texture elements is not as smooth."
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    setup_fog()


def main():
    window = Window(
        width=800, height=600, caption='Pyglet', resizable=True
    )
    # Hide the mouse cursor and prevent the mouse from leaving the window.
    window.set_exclusive_mouse(True)
    setup()
    app.run()


if __name__ == '__main__':
    main()
