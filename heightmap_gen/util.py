# "Neighborhoods",
# e.g. https://en.wikipedia.org/wiki/Von_Neumann_neighborhood
def moore(x,y,distance=1):
    return [
        (x-distance, y-distance),
        (x, y-distance),
        (x+distance, y-distance),
        (x-distance, y),
        (x+distance, y),
        (x-distance, y+distance),
        (x, y+distance),
        (x+distance, y+distance)
    ]


def neumann(x,y,distance=1):
    return [
        (x-distance,y),
        (x+distance,y),
        (x,y-distance),
        (x,y+distance)
    ]


def neumannr(x,y,distance=1): # rotated
    return [
        (x-distance,y-distance),
        (x+distance,y+distance),
        (x+distance,y-distance),
        (x-distance,y+distance)
    ]


### Color Ramp functions adpoted from Mudpie project:
# https://code.google.com/p/mudpie/
def geoRamp(waterLvl=90,snowLvl=228,grassLvl=(90,128),
treeLvl=(128,228)):
    result = []

    for i in range(waterLvl):
        result.append((92, 92, i + 128))

    for i in range(grassLvl[0],grassLvl[1]):
        result.append((64, i, 64))

    for i in range(treeLvl[0],treeLvl[1]):
        result.append((i,i,0))

    for i in range(snowLvl, 256):
        v = 128 + i // 2
        result.append((v,v,v))

    return result


def thermalRamp():
    """Shows much contrast between values"""
    result = []

    for i in range(64):
        result.append((0,0,i * 4))

    for i in range(64, 128):
        result.append((0, (i-64) * 4, 0))

    for i in range(128, 192):
        result.append(((i-128) * 4,0,0))

    for i in range(192, 256):
        result.append(((i-192)*4,(i-192)*4,0))

    return result


def landSeaRamp(sealevel=0.5):
    if type(sealevel) is float:
        sealevel = int(sealevel*256)

    c2 = sealevel // 2
    t = 256 - sealevel
    result = []

    for i in range(c2):
        result.append((0, 0, 255 * i // c2))

    for i in range(c2, sealevel):
        result.append((0, 255 * (i-c2) // c2, 255))

    for i in range(t):
        result.append((i * 255 // t, 128 + i * 128 // t, 0))

    return result


def fireRamp():
    result = []

    for i in range(128):
        result.append((i+64,0,0))

    for i in range(128, 256):
        result.append( (int(i/2+128), int((i-128)*3/2), 0) )

    return result


def zebraRamp():
    result = []

    for i in range(256):
        x = math.sin(i/16.0)
        x = math.floor((1.0-(x**2)) * i)
        result.append((x,i,x))

    return result
