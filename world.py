# Python native
from collections import deque
from time import time, perf_counter

# Pyglet
from pyglet import image
from pyglet.graphics import Batch, TextureGroup
from pyglet.gl import GL_QUADS

# Testcraft
from util import *
from terrain import getBlockStack

# A Batch is a collection of vertex lists for batched rendering.
batch = Batch()

texGroup = None

TEXTURE_FILE = 'texture.png'

BLOCK_TYPES = [
    tex_coords((1, 0), (0, 1), (0, 0)), # GRASS
    tex_coords((0, 1), (0, 1), (0, 1)), # DIRT
    tex_coords((0, 2), (0, 2), (0, 2)), # STONE
    tex_coords((1, 1), (1, 1), (1, 1)), # SAND
    tex_coords((2, 1), (2, 1), (2, 1)), # STONE_BRICK
    tex_coords((2, 0), (2, 0), (2, 0))  # BRICK
]

def world_init():
    global texGroup

    # A TextureGroup manages an OpenGL texture.
    texGroup = TextureGroup(
        image.load(TEXTURE_FILE).get_texture()
    )


FACES = [
    ( 0, 1, 0),
    ( 0,-1, 0),
    (-1, 0, 0),
    ( 1, 0, 0),
    ( 0, 0, 1),
    ( 0, 0,-1),
]

# A mapping from position to the texture of the block at that
# position. This defines all the blocks that are currently in
# the world.
worldBlocks = {}

# Same mapping as `world` but only contains blocks that are
# shown.
shown = {}

# Mapping from position to a pyglet `VertextList` for all shown
# blocks.
shownv = {}

# Mapping from sector to a list of positions inside that sector.
sectors = {}

# Simple function queue implementation. The queue is populated
# with show_block() and _hide_block() calls
queue = deque()


def hit_test(position, vector, max_distance=8):
    """Line of sight search from current position. If a block is
    intersected it is returned, along with the block previously in
    the line of sight. If no block is found, return None, None.

    Parameters
    ----------
    position : tuple of len 3
        The (x, y, z) position to check visibility from.
    vector : tuple of len 3
        The line of sight vector.
    max_distance : int
        How many blocks away to search for a hit.
    """
    m = 8
    x, y, z = position
    dx, dy, dz = vector
    previous = None

    for _ in range(max_distance * m):
        key = normalize((x, y, z))

        if key != previous and key in worldBlocks:
            return key, previous

        previous = key

        x, y, z = x + dx / m, y + dy / m, z + dz / m

    return None, None


def exposed(position):
    """Returns False if given `position` is surrounded on all 6
    sides by blocks, True otherwise.
    """
    x, y, z = position

    for fx, fy, fz in FACES:
        if (x + fx, y + fy, z + fz) not in worldBlocks:
            return True

    return False


def add_block(position, texture, immediate=True):
    """Add a block with the given `texture` and `position` to the
    world.

    Parameters
    ----------
    position : tuple of len 3
        The (x, y, z) position of the block to add.
    texture : list of len 3
        The coordinates of the texture squares. Use `tex_coords()`
        to generate.
    immediate : bool
        Whether or not to draw the block immediately.
    """
    if position in worldBlocks:
        remove_block(position, immediate)

    worldBlocks[position] = texture

    sectors.setdefault(
        sectorize(position), []
    ).append(position)

    if immediate:
        # ~ if exposed(position): # Shouldn't be here if not exposed
        show_block(position)

        check_neighbors(position)


def remove_block(position, immediate=True):
    """Remove the block at the given `position`.

    Parameters
    ----------
    position : tuple of len 3
        The (x, y, z) position of the block to remove.
    immediate : bool
        Whether or not to immediately remove block from canvas.
    """
    del worldBlocks[position]

    sectors[sectorize(position)].remove(position)

    if immediate:
        if position in shown:
            hide_block(position)

        check_neighbors(position)


def check_neighbors(position):
    """Check all blocks surrounding `position` and ensure their
    visual state is current. This means hiding blocks that are not
    exposed and ensuring that all exposed blocks are shown. Usually
    used after a block is added or removed.
    """
    x, y, z = position

    for fx, fy, fz in FACES:
        key = (x+fx, y+fy, z+fz)

        if key not in worldBlocks:
            continue

        if exposed(key):
            if key not in shown:
                show_block(key)
        else:
            if key in shown:
                hide_block(key)


def show_block(position, immediate=True):
    """Show the block at the given `position`. This method assumes
    the block has already been added with add_block()

    Parameters
    ----------
    position : tuple of len 3
        The (x, y, z) position of the block to show.
    immediate : bool
        Whether or not to show the block immediately.

    """
    texture = worldBlocks[position]

    shown[position] = texture

    if immediate:
        _show_block(position, texture)
    else:
        enqueue(_show_block, position, texture)


def _show_block(position, texture):
    """Private implementation of the `show_block()` method.

    Parameters
    ----------
    position : tuple of len 3
        The (x, y, z) position of the block to show.
    texture : list of len 3
        The coordinates of the texture squares. Use `tex_coords()`
        to generate.
    """
    vertex_data = cube_vertices(*position, 0.5)
    texture_data = list(texture)
    # create vertex list
    # ??? Maybe `add_indexed()` should be used instead
    # https://www.opengl-tutorial.org/intermediate-tutorials/tutorial-9-vbo-indexing/
    shownv[position] = batch.add(
        24,
        GL_QUADS,
        texGroup,
        ('v3f/static', vertex_data),
        ('t2f/static', texture_data)
    )


def hide_block(position, immediate=True):
    """Hide the block at the given `position`. Hiding does not
    remove the block from the world.

    Parameters
    ----------
    position : tuple of len 3
        The (x, y, z) position of the block to hide.
    immediate : bool
        Whether or not to immediately remove the block from the
        canvas.
    """
    shown.pop(position)

    if immediate:
        _hide_block(position)
    else:
        enqueue(_hide_block, position)


def _hide_block(position):
    """Private implementation of the 'hide_block()` method.
    """
    shownv.pop(position).delete()


def generate_sector(sector):
    blocks = []
    blocks_append = blocks.append
    sectors[sector] = blocks
    sx, sz = sector[0]*SECTOR_SIZE, sector[2]*SECTOR_SIZE

    # Replaced a nested for loop....Adopted from:
    # https://www.youtube.com/watch?v=vX4l-qozib8&lc=UgxB3pVuvd1AnZF3eNZ4AaABAg.9QBlOLeleDl9QD5UwjOZ2V
    # Timestamp 30:17

    for i in range(SECTOR_SIZE**2):
        x = floor(i / SECTOR_SIZE)+sx
        z = floor(i % SECTOR_SIZE)+sz

        blockStack = getBlockStack(x,z)

        surfaceBlock = blockStack[0]
        worldBlocks[surfaceBlock] = BLOCK_TYPES[0]
        blocks_append(surfaceBlock)
        show_block(surfaceBlock, False)

        for block in blockStack[1:]:
            worldBlocks[block] = BLOCK_TYPES[1]
            blocks_append(block)
            show_block(block, False)


def show_sector(sector):
    """Ensure all blocks in the given sector that should be shown
    are drawn to the canvas. If sector not in sectors,
    generates & caches a new one.

    NOTE: This is a memory leak, as nothing ever un-caches sectors.
    """
    if sector in sectors:
        for block in sectors[sector]:
            if block not in shown: #and exposed(block):
                show_block(block, False)
    else:
        enqueue(generate_sector, sector)
        # ~ generate_sector(sector)


def hide_sector(sector):
    """Ensure all blocks in the given sector that should be hidden
    are removed from the canvas.
    """
    for block in sectors.get(sector, []):
        if block in shown:
            hide_block(block, False)


def change_sectors(before, after):
    """Move from sector `before` to sector `after`. A sector is a
    contiguous x, y sub-region of world. Sectors are used to speed
    up world rendering.
    """
    before_set = set()
    after_set = set()

    for dx in range(-VIEW_DISTANCE, VIEW_DISTANCE+1):
            for dz in range(-VIEW_DISTANCE, VIEW_DISTANCE+1):
                # ~ if dx**2 + dy**2 + dz**2 > (VIEW_DISTANCE+1)**3:
                    # ~ print("penis")
                    # ~ continue

                if before:
                    before_set.add((before[0]+dx, 0, before[2]+dz))

                if after:
                    after_set.add((after[0]+dx, 0, after[2]+dz))

    for sector in after_set - before_set:
        show_sector(sector)

    for sector in before_set - after_set:
        hide_sector(sector)


def enqueue(func, *args):
    """Add `func` to the internal queue.
    """
    queue.append((func, args))


def dequeue():
    """Pop the top function from the internal queue and call it.
    """
    func, args = queue.popleft()
    func(*args)


def process_queue():
    """Process the entire queue while taking periodic breaks. This
    allows the game loop to run smoothly. The queue contains calls
    to _show_block() and _hide_block() so this method should be
    called if add_block() or remove_block() was called with
    immediate=False
    """
    start = perf_counter()

    while queue and perf_counter() - start < 1.0 / TICKS_PER_SEC:
        dequeue()


def process_entire_queue():
    """Process the entire queue with no breaks.
    """
    while queue:
        dequeue()
